# HTML static example

Toto je nejjednodušší možný příklad statického webu při použítí Auto-DevOps.cz.

## Konfigurace

Celá konfigurace je v souboru `.gitlab-ci.yml`:

```yaml
variables:
  AUTO_BUILD_PRESET: nginx-static
  AUTO_DEPLOY_BASE_DOMAIN: html-example.auto-devops.cz

include:
  - https://templates.auto-devops.cz/gitlab/latest/Auto-DevOps.gitlab-ci.yml
```

## Build

Pro sestavení aplikace se zde používá preset `nginx-static`, který udělá to že zveřejní všechny soubory ve
složce `public`. V tomto příkladu tedy vůbec není potřeba definovat vlastní Dockerfile.

## Deploy

Deploy je prováděn skrz SSH na VPS, přičem je zde definována pouze základní URL `html-example.auto-devops.cz`, vše
ostatní se děje automaticky samo.

### Prostředí

| Prostředí  | Funkce      | URL maska                                             | Poznámka                                                         |
|------------|-------------|-------------------------------------------------------|------------------------------------------------------------------|
| Review     | automaticky | `http://review<dynamic>.html-example.auto-devops.cz/` | Příklad http://review-button-123gam.html-example.auto-devops.cz/ |
| Dev        | automaticky | http://dev.html-example.auto-devops.cz/               |                                                                  |
| Staging    | vypnuto     | ---                                                   |                                                                  |
| Production | automaticky | http://html-example.auto-devops.cz/                   |                                                                  |
